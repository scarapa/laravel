(function ($) {
    $.fn.extend({
        cadastro: function (url) {

            var elemento = $(this);
            //var tamanhoArray = 0;

            //KEYUP
            elemento.keyup(function (ev) { });

            //CLICK
            elemento.click(function (ev) { });

            //CHANGE
            elemento.change(function (ev) {
                
                var geracao = $(this).attr('geracao');
                var proximaGeracao = parseInt(geracao) + 1;

                var arrayRetorno = buscarFilhos($(this).val())
                arrayRetorno = jQuery.parseJSON(arrayRetorno);

                if (arrayRetorno.length > 0) {
                    //1º LIMPAR TODOS OS SELECT INFERIORES A ATUAL
                    limparSelect(proximaGeracao);
                    //2º INJETAR NO SELECT DAQUELA GERACAO
                    montarSelect(arrayRetorno, proximaGeracao);
                }

            });

            function buscarFilhos(paiId) {
                var retorno = function () {
                    var tmp = null;
                    var url = "index.php?controller=cadastro&metodo=1&ajax=true&pai=" + paiId;
                    $.ajax({
                        'async': false,
                        'type': "POST",
                        'global': false,
                        'dataType': 'html',
                        'url': url,
                        'data': {'request': "", 'target': 'arrange_url', 'method': 'method_target'},
                        'success': function (data) {
                            tmp = data;
                        }
                    });
                    return tmp;
                }();
                return retorno;
            }
            function limparSelect(geracao) {
                var html = "<option value='0'>---Selecione---</option>";
                for (c = parseInt(geracao); c <= 7; c++) {
                    $("select[geracao=" + c + "]").empty();
                    $("select[geracao=" + c + "]").append(html);
                }
            }


            function montarSelect(array, geracao) {

                for (var i in array) {
                    //console.log(array[i].id +""+array[i].nome)
                    var html = "<option value='" + array[i].id + "'>" + array[i].nome + "</option>";
                    $("select[geracao=" + geracao + "]").append(html);
                }

            }

        }
    });
})(jQuery);