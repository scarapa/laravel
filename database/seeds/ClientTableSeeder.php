<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('clients')->delete();
        
        \App\Models\Client::create(['name' => 'Roberto Alam','birth' => '1981-11-15','email' => 'ralam@emater.tche.br']);
        \App\Models\Client::create(['name' => 'luisa Ferreira Alam','birth' => '2006-11-21','email' => 'luisaferreiraalam@gmail.com']);
        \App\Models\Client::create(['name' => 'Tonia Laura','birth' => '1985-03-10','email' => 'tonialcf@gmail.com']);

        \App\Models\Client::create(['name' => 'lizandro ciciliano tavares','birth' => '1983-06-27 ','email' => '']);
        \App\Models\Client::create(['name' => 'ana amelia damsceno cunha','birth' => '1985-08-15','email' => '']);
        \App\Models\Client::create(['name' => 'luiz eduardo valente','birth' => '1983-05-17','email' => '']);
        \App\Models\Client::create(['name' => 'eduardo schiller','birth' => '1982-05-23','email' => '']);
        \App\Models\Client::create(['name' => 'ane crochemore','birth' => '1984-01-03','email' => '']);
        \App\Models\Client::create(['name' => 'valdir farias bispo','birth' => '','email' => '']);
        \App\Models\Client::create(['name' => 'gabriela colvara ferreira','birth' => '','email' => '']);
        \App\Models\Client::create(['name' => 'vagner da costa ortiz','birth' => '1982-01-26','email' => '']);
        \App\Models\Client::create(['name' => 'cristina ceia ortiz','birth' => '','email' => '']);
        \App\Models\Client::create(['name' => 'leandro ferreira canhada','birth' => '1981-02-06','email' => 'lfc@gmail.com']);
        \App\Models\Client::create(['name' => 'natanael signorini','birth' => '1985-11-14','email' => '']);
        \App\Models\Client::create(['name' => 'michael ulguim','birth' => '1988-04-24','email' => 'michaelulguim@gmail.com']);
        \App\Models\Client::create(['name' => 'alice delias','birth' => '1986-12-29','email' => '']);
        \App\Models\Client::create(['name' => 'rafael bessa','birth' => '1982-07-14','email' => '']);
        \App\Models\Client::create(['name' => 'Guilherme Dambros','birth' => '1991-08-16','email' => 'gdambros@emater.tche.br']);
        \App\Models\Client::create(['name' => 'Jorge Titoneli','birth' => '1986-05-05','email' => 'jtitoneli@emater.tche.br']);
        \App\Models\Client::create(['name' => 'Douglas Ristow','birth' => '1980-04-24','email' => 'dristow@emater.tche.br']);
        \App\Models\Client::create(['name' => 'michel bueno giacobo','birth' => '1980-04-01','email' => 'mbueno@emater.tche.br']);
        \App\Models\Client::create(['name' => 'cristiano ramos moreira','birth' => '1977-04-28','email' => 'cmoreira@emater.tche.br']);
        \App\Models\Client::create(['name' => 'diogo coradini lopes','birth' => '1981-06-22','email' => 'dcoradini@emater.tche.br']);
        \App\Models\Client::create(['name' => 'marcos vinicius pizzuti nascimento','birth' => '1985-09-04','email' => 'mvnascimento@emater.tche.br']);
        \App\Models\Client::create(['name' => 'carine wontroba','birth' => '1987-10-07','email' => 'carinew@emater.tche.br']);
        \App\Models\Client::create(['name' => 'cicero antunes machado','birth' => '1975-01-16','email' => 'cicerom@emater.tche.br']);
        \App\Models\Client::create(['name' => 'liciane oliveira da silva','birth' => '1994-02-08','email' => 'losilva@emater.tche.br']);
        \App\Models\Client::create(['name' => 'anderson werkhauser marques','birth' => '1992-09-27','email' => 'awmarques@emater.tche.br']);        
        \App\Models\Client::create(['name' => 'angelica nezello','birth' => '1979-11-26','email' => 'anezello@emater.tche.br']);
        \App\Models\Client::create(['name' => 'andre mauricio lauer de souza','birth' => '1988-02-12','email' => 'asouza@emater.tche.br']);
        \App\Models\Client::create(['name' => 'celia carcuchinski','birth' => '1966-10-08','email' => 'celia@emater.tche.br']);
        \App\Models\Client::create(['name' => 'maria carmen seibel','birth' => '1959-12-07','email' => 'cseibel@emater.tche.br']);
        \App\Models\Client::create(['name' => 'heron hugo heinz','birth' => '1957-01-19','email' => 'hhh@emater.tche.br']);
        \App\Models\Client::create(['name' => 'alexandre lopes trindade','birth' => '1980-11-01','email' => 'atrindade@emater.tche.br']);
        \App\Models\Client::create(['name' => 'rodrigo thomas','birth' => '1987-10-18','email' => 'rthomas@emater.tche.br']);
        \App\Models\Client::create(['name' => 'jonatan elisandro fonseca pommer','birth' => '1988-04-03','email' => 'jpommer@emater.tche.br']);
        \App\Models\Client::create(['name' => 'pedro lopes pereira tonetto','birth' => '1993-07-14','email' => 'ptonetto@emater.tche.br  ']);
    }
}
