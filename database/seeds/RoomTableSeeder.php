<?php

use Illuminate\Database\Seeder;

class RoomTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('rooms')->delete();        
        
        \App\Models\Room::create(['name' => 'Sala Interna']);
        \App\Models\Room::create(['name' => 'Sala Externa']);
        \App\Models\Room::create(['name' => 'Sala Janta']);
    }
}
