<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('categories')->delete();        
        
        \App\Models\Category::create(['name' => 'Apartamento']);
        \App\Models\Category::create(['name' => 'Apartamento JK']);
        \App\Models\Category::create(['name' => 'Kitnet']);        
        \App\Models\Category::create(['name' => 'Sala']);
        \App\Models\Category::create(['name' => 'Loja']);
        \App\Models\Category::create(['name' => 'Casa']);
        \App\Models\Category::create(['name' => 'Sitio']);		
        \App\Models\Category::create(['name' => 'Fazenda']);
        \App\Models\Category::create(['name' => 'Terreno']);
    }
}
