<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('users')->delete();

        \App\User::create(['name'=>'Beto','email'=>'scarapa@gmail.com','password'=>bcrypt('123456')]);
        \App\User::create(['name'=>'Jorge','email'=>'jorge@gmail.com','password'=>bcrypt('123456')]);
        \App\User::create(['name'=>'Roberto','email'=>'ralam@emater.tche.br','password'=>bcrypt('123456')]);
    }
}
