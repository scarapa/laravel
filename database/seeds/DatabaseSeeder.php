<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // $this->call(UsersTableSeeder::class);
        $this->call('UserTableSeeder');
        $this->call('ClientTableSeeder');
        $this->call('CategoryTableSeeder');
        $this->call('RoomTableSeeder');
        $this->call('GarageTableSeeder');
    }

}
