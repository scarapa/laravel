<?php

use Illuminate\Database\Seeder;

class GarageTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('garages')->delete();
        
        \App\Models\Garage::create(['name' => 'Sem Carro']);
        \App\Models\Garage::create(['name' => '1 Carro']);
        \App\Models\Garage::create(['name' => '2 Carros']);
        \App\Models\Garage::create(['name' => '3 Carros']);
        \App\Models\Garage::create(['name' => '+3 Carros']);
    }

}
