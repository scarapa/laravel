<?php

use Illuminate\Database\Seeder;

class CharacteristicTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('characteristics')->delete();        
        
        \App\Models\Characteristic::create(['name' => 'Suite']);
        \App\Models\Characteristic::create(['name' => 'Pátio']);
        \App\Models\Characteristic::create(['name' => 'Banheira']);
        \App\Models\Characteristic::create(['name' => 'Lavanderia']);
        \App\Models\Characteristic::create(['name' => 'Escritório']);
        \App\Models\Characteristic::create(['name' => 'Despensa']);
        \App\Models\Characteristic::create(['name' => 'Churrasqueira']);
        \App\Models\Characteristic::create(['name' => 'Jardim']);
        \App\Models\Characteristic::create(['name' => 'Close']);
        \App\Models\Characteristic::create(['name' => 'Copa']);
        \App\Models\Characteristic::create(['name' => 'Piscina']);
    }
}
