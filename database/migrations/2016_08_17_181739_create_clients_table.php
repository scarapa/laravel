<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('clients', function(Blueprint $table){
            $table->increments('id');
            $table->string('name', 100);
            $table->date('birth');			
            $table->string('email', 200);
        });
    }

    public function down(){
        Schema::table('clients', function (Blueprint $table) {});
    }
}
