<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('properties', function (Blueprint $table) {
            $table->engine = 'InnoDB';            
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('business_id')->unsigned();
            $table->text('description');
            $table->string('cep', 8);
            $table->string('street', 255);
            $table->string('street_number', 10);
            $table->string('street_neighborhood', 30);
            $table->string('street_complement', 50);
            $table->string('street_reference', 150);			
            $table->string('city', 10);
            $table->double('area_total', 8, 2);
            $table->double('area_building', 8, 2);
            $table->integer('client_id')->unsigned()->nullable()->default(null);
            $table->foreign('client_id')->references('id')->on('clients');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->double('value', 15, 2);
            $table->text('observation');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::drop('properties');
    }
}
