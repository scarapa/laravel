@extends('sistema.template')
@section('content')
@include('sistema.template.navigation')
<hr>

<form id="formGenerico" name="formGenerico" method="post" action="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/store" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-lg-8">

            <div class="col-xs-6 col-lg-3">
                <label>Cliente:</label>
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <select id="cliente" name="cliente" class="form-control">
                        <option value="0">-- Selecione --</option>
                        <option value="420">Sergio</option>
                        <option value="181">Elizabete</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-6 col-lg-3">
                <label>Contrato:</label>
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <select id="contrato" name="contrato" class="form-control">
                        <option value="0">-- Selecione --</option>
                        <option value="1">Aluguel</option>
                        <option value="2">Venda</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-6 col-lg-3">
                <label>Categoria:</label>
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <select id="contrato" name="contrato" class="form-control">
                        <option value="0">-- Selecione --</option>
                    </select>
                </div>
            </div>            

            <div class="col-xs-6 col-lg-3">
                <label>Valor R$:</label>
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                    <input type="text" value="" name="valor" id="valor" placeholder="R$" class="form-control dinheiro">
                </div>
            </div>            
        </div>

        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">SALAS</div>
                <div class="panel-body">
                    <label class="checkbox-inline"><input type="checkbox" name="sala[Sala Interna]">Sala Interna</label>                    
                    <label class="checkbox-inline"><input type="checkbox" name="sala[Sala Externa]">Sala Externa</label>                    
                    <label class="checkbox-inline"><input type="checkbox" name="sala[Sala Jantar]">Sala Jantar</label>                    

                </div>

                <div class="panel-heading">CARACTERISTICAS</div>
                <div id="caracteristica" class="panel-body">
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Suite]">Suite </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Churrasqueira]">Churrasqueira </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Patio]">Patio </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Jardim]">Jardim </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Banheira]">Banheira </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Close]">Close </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Lavanderia]">Lavanderia </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Copa]">Copa </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Escritorio]">Escritorio </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Piscina]">Piscina </label>
                    <label class="checkbox-inline"><input type="checkbox" name="caracteristica[Despensa]">Despensa </label>
                </div>

                <div class="panel-heading">SITUAÇÃO</div>
                <div class="panel-body">
                    <label class="checkbox-inline"><input type="checkbox" name="situacao[Ocupado]">Ocupado</label>                    
                    <label class="checkbox-inline"><input type="checkbox" name="situacao[Exclusivo]">Exclusivo</label>                    
                </div>
            </div>
        </div>
    </div>
    <hr style="width:100%">

    <div class="form-group form-buttons">
        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Salvar</button>
        <a class="btn btn-danger" href="#">Cancelar</a>
    </div>
</form>
@stop