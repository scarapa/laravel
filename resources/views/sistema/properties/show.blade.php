@extends('sistema.template')
@section('content')
@include('sistema.template.navigation')
<hr>
    <div class="row">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-lg-3">
            <div class="form-group">
                <label class="control-label">Nome<span class="required">*</span></label>
                <input type="text" placeholder="Nome" class="form-control" name="name" value="{{$data->name}}" disabled="">
            </div>
        </div>

        <div class="col-lg-3">
            <div class="form-group">
                <label class="control-label">Nascimento<span class="required">*</span></label>
                <input type="text" placeholder="0000-00-00" class="form-control" name="nascimento" value="{{$data->nascimento}}" disabled="">
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label class="control-label">Email<span class="required">*</span></label>
                <input type="text" placeholder="Email" class="form-control" name="email" value="{{$data->email}}" disabled="">
            </div>
        </div>                            

        <hr>
    </div>
</div>   
@stop