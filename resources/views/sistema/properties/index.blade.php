@extends('sistema.template')
@section('content')
@include('sistema.template.navigation')
<hr>
<div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="grid">
        <thead>
            <tr>
                <th style="width:5%" class="hidden-xs">COD</th>
                <th>Categoria</th>
                <th>Negócio</th>
                <th>Bairro</th>
                <th>Valor</th>                
                <th style="width:17%"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $valor)
                <tr>
                    <td>{{ $valor->id }}</td>
                    <td>{{ $valor->category_description}}</td>
                    <td>{{ $valor->business_description }}</td>
                    <td>{{ $valor->neig }}</td>
                    <td>{{ $valor->value }}</td>                    
                    <td>
                        <a href="{{ URL::to(Request::segments()[0].'/'.Request::segments()[1].'/'.$valor->id.'/edit') }}" class="btn btn-default btn-xs button-action" title="Editar">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <a href="{{ URL::to(Request::segments()[0].'/'.Request::segments()[1].'/'.$valor->id . '/show') }}" class="btn btn-default btn-xs button-action" title="Exibir">
                            <span class="fa fa-search"></span>
                        </a>
                        <a href="{{ URL::to(Request::segments()[0].'/'.Request::segments()[1].'/'.$valor->id . '/delete') }}" class="btn btn-default btn-xs button-action" title="Remover">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>    

<script></script>
@stop
