
    <nav role="navigation" class="navbar navbar-default">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a href="#" class="navbar-brand">
                    <div class="icon fa fa-building"></div>
                    <div class="title">SISTEMA</div>
                </a>
                <button class="navbar-expand-toggle pull-right visible-xs" type="button">
                    <i class="fa fa-times icon"></i>
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{URL::to('/')}}/sistema">
                        <span class="icon fa fa-tachometer"></span>
                        <span class="title">Painel</span>
                    </a>
                </li>
                
                <li class="panel panel-default dropdown">
                    <a href="#dropdown-imovel" data-toggle="collapse">
                        <span class="icon fa fa-building fa-fw"></span><span class="title">Imoveis</span>
                    </a>

                    <div class="panel-collapse collapse" id="dropdown-imovel">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="{{URL::to('/')}}/sistema/properties/index">Imoveis</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('/')}}/sistema/categories/index">Categorias</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>                
                
                <li>
                    <a href="{{URL::to('/')}}/sistema/clients/index">
                        <span class="icon fa fa-user fa-fw"></span>
                        <span class="title">Clientes</span>
                    </a>
                </li>

                <li class="panel panel-default dropdown">
                    <a href="#dropdown-usuario" data-toggle="collapse">
                        <span class="icon fa fa-users fa-fw"></span><span class="title">Usuarios</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div class="panel-collapse collapse" id="dropdown-usuario">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="{{URL::to('/')}}/sistema/users/index">Usuario</a>
                                </li>
                                <li>
                                    <a href="index.php?controller=grupo">Grupo</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                
                <li class="panel panel-default dropdown">
                    <a href="#dropdown-cadastro" data-toggle="collapse">
                        <span class="icon fa fa-edit  fa-fw"></span><span class="title">Cadastro</span>
                    </a>

                    <div class="panel-collapse collapse" id="dropdown-cadastro">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="{{URL::to('/')}}/sistema/register/rooms/index">Salas</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('/')}}/sistema/register/garages/index">Garagem</a>
                                </li>                                
                                <li>
                                    <a href="{{URL::to('/')}}/sistema/register/categories/index">Categorias</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>                  
            </ul>
        </div>
    </nav>
