@extends('sistema.template')
@section('content')
@include('sistema.template.navigation')
<hr>
    <div class="row">
        <form id="formGenerico" name="formGenerico" method="post" action="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/update" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{Request::segments()[2]}}">
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="control-label">Nome<span class="required">*</span></label>
                    <input type="text" placeholder="Nome" class="form-control" name="name" value="{{$data->name}}">
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="control-label">Nascimento<span class="required">*</span></label>
                    <input type="text" placeholder="0000-00-00" class="form-control" name="nascimento" value="{{$data->birth}}">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="control-label">Email<span class="required">*</span></label>
                    <input type="text" placeholder="Email" class="form-control" name="email" value="{{$data->email}}">
                </div>
            </div>                            

            <hr>

            <div class="form-group form-buttons">
                <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <a class="btn btn-danger" href="#">Cancelar</a>
            </div>

        </form>

    </div>
</div>   
@stop