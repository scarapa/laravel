<div class="row">
    <div class="page-buttons pull-left">
        <a class="btn btn-primary" href="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/{{Request::segments()[2]}}/index">
            <i class="fa fa-list"></i> Listar
        </a>
        <a class="btn btn-success" href="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/{{Request::segments()[2]}}/create">
            <i class="fa fa-plus"></i> Adicionar
        </a>
    </div>
</div>
