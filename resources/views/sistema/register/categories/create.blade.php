@extends('sistema.template')
@section('content')
@include('sistema.register.navigation')
<hr>
    <form id="formGenerico" name="formGenerico" method="post" action="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/{{Request::segments()[2]}}/store" >
        <div class="row">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="control-label">Nome<span class="required">*</span></label>
                    <input type="text" placeholder="Nome" class="form-control" name="name">
                </div>
            </div>

        </div>

        <div class="form-group form-buttons">
            <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Salvar</button>
            <a class="btn btn-danger" href="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/{{Request::segments()[2]}}/index">Cancelar</a>
        </div>

    </form>

@stop