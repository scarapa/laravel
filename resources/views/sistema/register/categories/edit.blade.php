@extends('sistema.template')
@section('content')
@include('sistema.register.navigation')
<hr>
    <div class="row">
        <form id="formGenerico" name="formGenerico" method="post" action="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/{{Request::segments()[2]}}/update" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{Request::segments()[3]}}">            
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="control-label">Nome<span class="required">*</span></label>
                    <input type="text" placeholder="Nome" class="form-control" name="name" value="{{$data->name}}">
                </div>
            </div>
        </form>
    </div>
@stop