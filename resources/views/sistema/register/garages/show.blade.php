@extends('sistema.template')
@section('content')
@include('sistema.register.navigation')
<hr>
    <div class="row">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-lg-3">
            <div class="form-group">
                <label class="control-label">Nome<span class="required">*</span></label>
                <input type="text" placeholder="Nome" class="form-control" name="name" value="{{$data->name}}" disabled="">
            </div>
        </div>
    </div>   
@stop