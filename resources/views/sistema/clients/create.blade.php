@extends('sistema.template')
@section('content')
@include('sistema.template.navigation')
<hr>
    
    <div class="row">
        <form id="formGenerico" name="formGenerico" method="post" action="{{URL::to('/')}}/{{Request::segments()[0]}}/{{Request::segments()[1]}}/store" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="control-label">Nome<span class="required">*</span></label>
                    <input type="text" placeholder="Nome" class="form-control" name="name">
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label class="control-label">Nascimento<span class="required">*</span></label>
                    <input type="text" class="form-control data" name="birth">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label class="control-label">Email<span class="required">*</span></label>
                    <input type="text" placeholder="Email" class="form-control" name="email">
                </div>
            </div>                            


            <hr>


            <div class="form-group form-buttons">
                <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <a class="btn btn-danger" href="#">Cancelar</a>
            </div>

        </form>

    </div>

@stop