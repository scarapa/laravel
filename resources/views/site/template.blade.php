<!DOCTYPE html>
<html>
    <head>
        <title>SITE - Imobiliaria</title>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="imagem/favicon.ico" type="image/x-icon">
        <link rel="icon" href="imagem/favicon.ico" type="image/x-icon">    
        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/roboto.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/lato.css">
        <!-- CSS Libs -->
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/bootstrap-switch.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/checkbox3.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/dataTables.bootstrap.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/select2.min.css">
        <!-- CSS App -->
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/style.css">
        <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/css/themes/flat-blue.css">
        <script type="text/javascript" src="{{URL::to('/')}}/js/jquery.min.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/bootstrap.min.js"></script>
        <style></style>
    </head>

    <body class="flat-blue">
        teste
        </div>
	
        <script type="text/javascript" src="{{URL::to('/')}}/js/Chart.min.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/bootstrap-switch.min.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/select2.full.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/ace.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/mode-html.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/theme-github.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/helper.js"></script>    
        <script type="text/javascript" src="{{URL::to('/')}}/js/app.js"></script>    
        <script type="text/javascript" src="{{URL::to('/')}}/js/index.js"></script>
        <script type="text/javascript" src="{{URL::to('/')}}/js/chartjs.js"></script>            
    </body>
</html>
