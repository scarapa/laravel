<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/* SITE */
Route::get('/', 'SiteController@index');
Route::get('site/', 'SiteController@index');
Route::get('site/imoveis', 'SiteController@imoveis');
Route::get('site/imovel/{id}', 'SiteController@imovel');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

Route::group(['middleware' => 'auth'], function() {
    //SISTEMA RAIZ
    Route::get('sistema/', 'SistemaController@index');
    /* PROPERTIES */
    Route::get('sistema/properties/', 'PropertiesController@index');
    Route::get('sistema/properties/index', 'PropertiesController@index');
    Route::get('sistema/properties/create', 'PropertiesController@create');
    Route::get('sistema/properties/{id}/show', 'PropertiesController@show');
    Route::get('sistema/properties/{id}/edit', 'PropertiesController@edit');
    Route::post('sistema/properties/update', 'PropertiesController@update');
    Route::get('sistema/properties/{id}/delete', 'PropertiesController@destroy');
    Route::post('sistema/properties/store', 'PropertiesController@store');    
    /* USERS */
    Route::get('sistema/users/', 'UsersController@index');
    Route::get('sistema/users/index', 'UsersController@index');
    Route::get('sistema/users/create', 'UsersController@create');
    Route::get('sistema/users/{id}/show', 'UsersController@show');
    Route::get('sistema/users/{id}/edit', 'UsersController@edit');
    Route::post('sistema/users/update', 'UsersController@update');
    Route::get('sistema/users/{id}/delete', 'UsersController@destroy');
    Route::post('sistema/users/store', 'UsersController@store');    
    /* CLIENTS */
    Route::get('sistema/clients/', 'ClientsController@index');
    Route::get('sistema/clients/index', 'ClientsController@index');
    Route::get('sistema/clients/create', 'ClientsController@create');
    Route::get('sistema/clients/{id}/show', 'ClientsController@show');
    Route::get('sistema/clients/{id}/edit', 'ClientsController@edit');
    Route::post('sistema/clients/update', 'ClientsController@update');
    Route::get('sistema/clients/{id}/delete', 'ClientsController@destroy');
    Route::post('sistema/clients/store', 'ClientsController@store');
    /* CATEGORIES */
    Route::get('sistema/register/categories/', 'CategoriesController@index');
    Route::get('sistema/register/categories/index', 'CategoriesController@index');
    Route::get('sistema/register/categories/create', 'CategoriesController@create');
    Route::get('sistema/register/categories/{id}/show', 'CategoriesController@show');
    Route::get('sistema/register/categories/{id}/edit', 'CategoriesController@edit');
    Route::post('sistema/register/categories/{id}/update', 'CategoriesController@update');
    Route::get('sistema/register/categories/{id}/delete', 'CategoriesController@destroy');
    Route::post('sistema/register/categories/store', 'CategoriesController@store');
    /* CATEGORIES */
    Route::get('sistema/register/rooms/', 'RoomsController@index');
    Route::get('sistema/register/rooms/index', 'RoomsController@index');
    Route::get('sistema/register/rooms/create', 'RoomsController@create');
    Route::get('sistema/register/rooms/{id}/show', 'RoomsController@show');
    Route::get('sistema/register/rooms/{id}/edit', 'RoomsController@edit');
    Route::post('sistema/register/rooms/{id}/update', 'RoomsController@update');
    Route::get('sistema/register/rooms/{id}/delete', 'RoomsController@destroy');
    Route::post('sistema/register/rooms/store', 'RoomsController@store');    
    /* GARAGES */
    Route::get('sistema/register/garages/', 'GaragesController@index');
    Route::get('sistema/register/garages/index', 'GaragesController@index');
    Route::get('sistema/register/garages/create', 'GaragesController@create');
    Route::get('sistema/register/garages/{id}/show', 'GaragesController@show');
    Route::get('sistema/register/garages/{id}/edit', 'GaragesController@edit');
    Route::post('sistema/register/garages/{id}/update', 'GaragesController@update');
    Route::get('sistema/register/garages/{id}/delete', 'GaragesController@destroy');
    Route::post('sistema/register/garages/store', 'GaragesController@store');        
});


Route::auth();
//Route::get('/home', 'HomeController@index');