<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Models\Category;
use App\Abstracts\Funcoes;

class CategoriesController extends Controller {

    private $controller = "categories";
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $object = new Category();
        $data = $object::all();
        return view('sistema.register.'.$this->controller.'.index',  compact('data'));
    }

    public function create() {
        return view('sistema.register.'.$this->controller.'.create');
    }

    public function store(Request $request){
        $object = new Category();

        $nome = $request['name'];

        $object->name = $nome;
        $object->save();

        return redirect('sistema/register/'.$this->controller );
    }

    public function show($id){
        $object = new Category();
        $data = $object::find($id);
        return view('sistema.register.'.$this->controller.'.show', compact('data'));
    }

    public function edit($id){
        $object = new Category();
        $data = $object::find($id);
        return view('sistema.register.'.$this->controller.'.edit', compact('data'));
    }

    public function update(Request $request,$id){

        $object = new Category();            
        $loadObject = $object::find($id);

        $loadObject->name = $request['name'];
        $loadObject->update();

        return redirect('sistema/register/'.$this->controller );
    }

    public function destroy($id){
        $object = new Category();            
        $loadObject = $object::find($id);
        $loadObject->delete();

        return redirect('sistema/register/'.$this->controller );
    }

}