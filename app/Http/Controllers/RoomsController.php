<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class RoomsController extends Controller{
    
    private $controller = "rooms";
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $object = new Room();
        $data = $object::all();
        return view('sistema.register.'.$this->controller.'.index',  compact('data'));
    }
}
