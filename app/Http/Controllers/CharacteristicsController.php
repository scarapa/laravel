<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Characteristic;
class CharacteristicsController extends Controller{

    private $controller = "characteristics";
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){
        $object = new Characteristics();
        $data = $object::all();
        return view('sistema.register.'.$this->controller.'.index',  compact('data'));
    }

    public function create() {
        return view('sistema.register.'.$this->controller.'.create');
    }

    public function store(Request $request){
        $object = new Characteristics();

        $nome = $request['name'];

        $object->name = $nome;
        $object->save();

        return redirect('sistema/register/'.$this->controller );
    }

    public function show($id){
        $object = new Characteristics();
        $data = $object::find($id);
        return view('sistema.register.'.$this->controller.'.show', compact('data'));
    }

    public function edit($id){
        $object = new Characteristics();
        $data = $object::find($id);
        return view('sistema.register.'.$this->controller.'.edit', compact('data'));
    }

    public function update(Request $request,$id){

        $object = new Characteristics();            
        $loadObject = $object::find($id);

        $loadObject->name = $request['name'];
        $loadObject->update();

        return redirect('sistema/register/'.$this->controller );
    }

    public function destroy($id){
        $object = new Characteristics();            
        $loadObject = $object::find($id);
        $loadObject->delete();

        return redirect('sistema/register/'.$this->controller );
    }

}