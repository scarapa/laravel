<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\User;
use App\Abstracts\Funcoes;

class UsersController extends Controller {

    private $controller = "users";
    
    public function __construct(){
        $this->middleware('web');
    }
    
    public function index(){
        $object = new User();
        $data = $object::all();
        return view('sistema.'.$this->controller.'.index',  compact('data'));
    }

    public function create() {
        return view('sistema.'.$this->controller.'.create');
    }

    public function store(Request $request){
        $object = new User();
        $nome = $request['name'];
        $nascimento = Funcoes::DataBrToEUA($request['birth']);
        $email = $request['email'];

        $object->name = $nome;
        $object->birth = $nascimento;
        $object->email = $email;
        $object->save();

        return redirect('sistema/'.$this->controller );
    }

    public function show($id){
        $object = new User();
        $data = $object::find($id);
        $data->birth = Funcoes::DataEUAToBr($data->birth);        
        return view('sistema.'.$this->controller.'.show', compact('data'));
    }

    public function edit($id){
        $object = new User();
        $data = $object::find($id);
        $data->birth = Funcoes::DataEUAToBr($data->birth);
        return view('sistema.'.$this->controller.'.edit', compact('data'));
    }

    public function update(){
        $object = new User();            
        $loadObject = $object::find($_POST['id']);
        
        $loadObject->name = $_POST['name'];
        $loadObject->birth = Funcoes::DataBrToEUA($_POST['nascimento']);
        $loadObject->email = $_POST['email'];
        $loadObject->update();
        return redirect('sistema/'.$this->controller );
    }

    public function destroy($id){
        $object = new User();            
        $loadObject = $object::find($id);
        $loadObject->delete();
        return redirect('sistema/'.$this->controller );
    }

}