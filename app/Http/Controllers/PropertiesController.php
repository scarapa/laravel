<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Models\Property;
use App\Models\Client;

class PropertiesController extends Controller {

    private $controller = "properties";
    
    public function __construct(){
        $this->middleware('web');
    }
    
    public function index(){
        $object = new Property();
        $data = $object::all();
        return view('sistema.'.$this->controller.'.index',  compact('data'));
    }

    public function create() {
        return view('sistema.'.$this->controller.'.create');        
    }

    public function store(Request $request){
        $object = new Property();
        $nome = $request['name'];
        $nascimento = $request['birth'];
        $email = $request['email'];

        $object->name = $nome;
        $object->birth = $nascimento;
        $object->email = $email;
        $object->save();

        return redirect('sistema/'.$this->controller );
    }

    public function show($id){
        $object = new Property();
        $data = $object::find($id);
        return view('sistema.'.$this->controller.'.show', compact('data'));
    }

    public function edit($id){
        $object = new Property();
        $data = $object::find($id);
        
        return view('sistema.'.$this->controller.'.edit', compact('data'));
    }

    public function update(){
        $object = new Property();            
        $loadObject = $object::find($_POST['id']);

        $loadObject->name = $_POST['name'];
        $loadObject->birth = $_POST['nascimento'];
        $loadObject->email = $_POST['email'];
        $loadObject->update();
        return redirect('sistema/'.$this->controller );
    }

    public function destroy($id){
        $object = new Property();            
        $loadObject = $object::find($id);
        $loadObject->delete();
        return redirect('sistema/'.$this->controller );
    }

}