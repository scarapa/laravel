<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class SistemaController extends Controller{
    public function __construct(){
        $this->middleware('web');
    }    
    
    public function index(){
        return view('sistema/dashboard/index');
    }

    public function home(){
        return view('site/home');
    }    
    
    public function imoveis(){
        return view('site/imoveis');
    }        
    
    public function imovel(){
        return view('site/imovel');
    }
    
    public function empresa(){
        return view('site/empresa');
    }
    
    public function contato(){
        return view('site/contato');
    } 
}
