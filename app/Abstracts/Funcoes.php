<?php
namespace App\Abstracts;

Class Funcoes {

    public static function DataBrToEUA($date) {
        $data = explode("/", $date);
        $data = $data[2] . "-" . $data[1] . "-" . $data[0];
        return $data;
    }

    public static function DataEUAToEUA($date) {
        $data = explode("-", $date);
        $data = $data[2] . "-" . $data[1] . "-" . $data[0];
        //$data = "10/03/1985";
        return $data;
    }

    public static function DataEUAToBr($date) {
        $data = explode("-", $date);
        $data = $data[2] . "/" . $data[1] . "/" . $data[0];
        return $data;
    }

    public static function dataAtual() {
        return date("d/m/Y");
    }

    public static function horaAtual() {
        return date("H:i:s");
    }

    public static function numerosPagina($registroTotal, $registroPagina) {
        return ceil($registroTotal / $registroPagina);
    }

    public static function soNumeros($variavel) {
        $c = strlen($variavel);
        $v = "";$t="";
        for ($i = 0; $i < $c; $i++) {
            $t = $variavel[$i];
            if (is_numeric($t)) { $v = $v.$t; }
        }
        return $v;
    }
    
    public static function arquivoTamanho($parametro) {
    /* Medidas */
        $medidas = array('KB', 'MB', 'GB', 'TB');

        /* Se for menor que 1KB arredonda para 1KB */
        if($parametro < 999){
            $parametro = 1000;
        }

        for ($i = 0; $parametro > 999; $i++){
            $parametro /= 1024;
        }

        return round($parametro) . $medidas[$i - 1];
    }

}
