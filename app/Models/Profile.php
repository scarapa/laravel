<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Profile extends User{
    protected $table = "profiles";    
    protected $fillable = ['group','name','image'];
    public $timestamps = false;    
}
