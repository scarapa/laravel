<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Garage extends Model{
    protected $table = "garages";    
    protected $fillable = ['name'];
    public $timestamps = false;   
}
